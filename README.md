This is my semester project for CptS 437 Introduction to Machine Learning
with Dr. Diane Cook in the Spring 2020 semester.

This project uses machine learning algorithms to view seismic bump data and
attempt to determine a pattern that will be able to accurately predict when
an earthquake will occur.