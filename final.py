#remove annoying warnings
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

# import libraries
import csv
import numpy as np
import matplotlib.pyplot as plt
# from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import MultinomialNB, GaussianNB
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, BaggingClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import LabelEncoder
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.utils import shuffle
# from google.colab import drive
# drive.mount('/content/drive/')
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

X = []
y = []
tot = []

# open up the data file for parsing
index = 0
with open('./data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        # the data starts at line 155
        # if index >= 154:
        X.append([str(i) for i in row[:-1]])
        y.append(row[-1])
        # print(X[-1])
        # print(y[-1])
        # break
        # index += 1
        tot.append(row)

# returns the scores for each of the classifiers. tot is the total data being passed in. method is ensemble method used (default is None).
def return_scores(tot, method=None):
    global X
    global y
    tot = shuffle(tot)

    for row in tot:
        X.append([str(i) for i in row[:-1]])
        y.append(row[-1])

    # convert text to numbers so they can be inserted into a sklearn classifier.
    new_X = []
    enc = LabelEncoder()
    # for l in X:
    #     for i in range(len(l)):
    #         print(l[i])
    #         enc.fit(l[i])
    #         l[i] = enc.transform(l[i])
    #     print(l)
    #     break
    for l in X:
        enc.fit(l)
        l = enc.transform(l)
        new_X.append(l)
    # print(new_X)
    X = new_X

    # data needs to be an numpy array in order to split it with sklearn KFold
    # X = np.array(X)
    # y = np.array(y)

    # split the data up into multiple folds for cross validation
    # kf = KFold(n_splits=10)
    # for train_index, test_index in kf.split(X):
    #     print("TRAIN:", train_index, "TEST:", test_index)
    #     X_train, X_test = X[train_index], X[test_index]
    #     y_train, y_test = y[train_index], y[test_index]

    # create train test split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
    # print(X_train, y_train)

    # svm classifier
    svm = SVC() if method == None else method(SVC(), param_grid={'C': [1, 10], 'kernel': ('linear', 'rbf')})
    svm.fit(X_train, y_train)
    svm_score = svm.score(X_test, y_test)
    print('SVM score: ', svm.score(X_test, y_test))

    # Gaussian naive bayes classifier
    bayes = GaussianNB() if method == None else method(GaussianNB(), param_grid = {'var_smoothing': [1*(10**(-9)), 2*(10**(-9))]})
    bayes.fit(X_train, y_train)
    bayes_score = bayes.score(X_test, y_test)
    print('Gaussian naive bayes score: ', bayes.score(X_test, y_test))

    # Gaussian process classifier
    # gauss1 = GaussianProcessClassifier() if method == None else method(GaussianProcessClassifier())
    # gauss1.fit(X_train, y_train)
    # gauss1_score = gauss1.score(X_test, y_test)
    # print('Gaussian process #1 score: ', gauss1.score(X_test, y_test))

    # Gaussian process classifier
    # gauss2 = GaussianProcessClassifier(1.0 * RBF(1.0)) if method == None else method(GaussianProcessClassifier(1.0 * RBF(1.0)))
    # gauss2.fit(X_train, y_train)
    # gauss2_score = gauss2.score(X_test, y_test)
    # print('Gaussian process #2 score: ', gauss2.score(X_test, y_test))

    # Quadratic discriminant analysis classifier
    neg = 1e-10
    quad = QuadraticDiscriminantAnalysis() if method == None else method(QuadraticDiscriminantAnalysis(), param_grid = {'reg_param' : [0.0, 0.1, 0.01], 'tol' : [0.0001, 0.0003, 0.0005, 0.0007]})
    quad.fit(X_train, y_train)
    quad_score = quad.score(X_test, y_test)
    print('Quadratic discriminant analysis score: ', quad.score(X_test, y_test))

    # neural network classifier
    parameters={
        'learning_rate': ["constant", "invscaling", "adaptive"],
        'hidden_layer_sizes': [(10, 30, 20)],
        'alpha': [1*(10**-5), 1.3*(10**-5), 1.5*(10**-5)],
        'activation': ["logistic", "relu", "tanh"]
    }
    net = MLPClassifier() if method == None else method(MLPClassifier(), param_grid = parameters)
    net.fit(X_train, y_train)
    net_score = net.score(X_test, y_test)
    print('Neural network score: ', net.score(X_test, y_test))

    # random forest classifier
    forest = RandomForestClassifier() if method == None else method(RandomForestClassifier(), param_grid = {'n_estimators': [200, 700], 'max_features': ['auto', 'sqrt', 'log2']})
    forest.fit(X_train, y_train)
    forest_score = forest.score(X_test, y_test)
    print('Random forest score: ', forest.score(X_test, y_test))

    # ada boost classifier
    parameters = {
        # "base_estimator__criterion" : ["gini", "entropy"],
        # "base_estimator__splitter" :   ["best", "random"],
        "n_estimators": [1, 2]
    }
    boost = AdaBoostClassifier() if method == None else method(AdaBoostClassifier(), param_grid = parameters)
    boost.fit(X_train, y_train)
    boost_score = boost.score(X_test, y_test)
    print('Ada boost score: ', boost.score(X_test, y_test))

    # return svm_score, bayes_score, gauss1_score, gauss2_score, quad_score, net_score, forest_score, boost_score
    # return svm_score, bayes_score, gauss1_score, quad_score, net_score, forest_score, boost_score
    return svm_score, bayes_score, quad_score, net_score, forest_score, boost_score

# finds the average scores of the classifiers and plots each iteration of the classifiers score on a graph. method is the ensemble method used (default None).
def make_graph(method=None):
    svm_scores = []
    bayes_scores = []
    # gauss1_scores = []
    # gauss2_scores = []
    quad_scores = []
    net_scores = []
    forest_scores = []
    boost_scores = []

    # this loop determines how many times the data will be shuffled and then trained on.
    for i in range(6):
        print("\niteration #", i)
        # svm_score, bayes_score, gauss1_score, gauss2_score, quad_score, net_score, forest_score, boost_score = return_scores(tot)
        # svm_score, bayes_score, gauss1_score, quad_score, net_score, forest_score, boost_score = return_scores(tot)
        svm_score, bayes_score, quad_score, net_score, forest_score, boost_score = return_scores(tot) if method == None else return_scores(tot, method)
        svm_scores.append(svm_score)
        bayes_scores.append(bayes_score)
        # gauss1_scores.append(gauss1_score)
        # gauss2_scores.append(gauss2_score)
        quad_scores.append(quad_score)
        net_scores.append(net_score)
        forest_scores.append(forest_score)
        boost_scores.append(boost_score)
    print('\n')

    # used to find the average scores of each classifier
    def find_avg(arr):
        total = 0
        num = 0
        for i in arr:
            num += i
            total += 1
        return num / total

    # the average scores get appended to this array
    avgs = []
    avgs.append(find_avg(svm_scores))
    avgs.append(find_avg(bayes_scores))
    # avgs.append(find_avg(gauss1_scores))
    # avgs.append(find_avg)gauss2_scores))
    avgs.append(find_avg(quad_scores))
    avgs.append(find_avg(net_scores))
    avgs.append(find_avg(forest_scores))
    avgs.append(find_avg(boost_scores))

    # the following code finds the highest scoring (on average) classifier and prints it to the terminal.
    scores = {0: 'svm', 
            1: 'bayes', 
            #   2: 'gauss1', 
            #   3: 'gauss2', 
            2: 'quad', 
            3: 'net', 
            4: 'forest', 
            5: 'boost'}
    max_avg = 0
    index = 0
    for i in avgs:
        if i > max_avg:
            max_avg = i
            max_avg_name = scores[index]
        index += 1

    print(max_avg_name, 'is the classifier with the highest average score with an average score of', max_avg)

    # Create a graph that shows the scores of each classifier for each iteration
    data = [[svm_scores[0], bayes_scores[0], quad_scores[0], net_scores[0], forest_scores[0], boost_scores[0]],
            [svm_scores[1], bayes_scores[1], quad_scores[1], net_scores[1], forest_scores[1], boost_scores[1]],
            [svm_scores[2], bayes_scores[2], quad_scores[2], net_scores[2], forest_scores[2], boost_scores[2]],
            [svm_scores[3], bayes_scores[3], quad_scores[3], net_scores[3], forest_scores[3], boost_scores[3]],
            [svm_scores[4], bayes_scores[4], quad_scores[4], net_scores[4], forest_scores[4], boost_scores[4]],
            [svm_scores[5], bayes_scores[5], quad_scores[5], net_scores[5], forest_scores[5], boost_scores[5]]]
    columns = ("svm", "Gaussian\nbayes", "Quadratic\ndiscriminant\nanalysis", "Neural\nnetwork", "Random\nforest", "Ada\nboost")
    rows = ['iteration %d' % x for x in range(6)]

    values = np.arange(0, 6, 1)

    value_increment = 1

    # Get some pastel shades for the colors
    colors = plt.cm.BuPu(np.linspace(0, 0.5, len(rows)))
    n_rows = len(data)

    index = np.arange(len(columns)) + 0.3
    bar_width = 0.4

    # Initialize the vertical-offset for the stacked bar chart.
    y_offset = np.zeros(len(columns))

    # Plot bars and create text labels for the table
    cell_text = []
    for row in range(n_rows):
        # print(index)
        # print(data[row])
        plt.bar(index, data[row], bar_width, bottom=y_offset, color=colors[row])
        y_offset = y_offset + data[row]
        cell_text.append(['%f' % (x / 1) for x in data[row]])
    # Reverse colors and text labels to display the last value at the top.
    colors = colors[::-1]
    cell_text.reverse()

    # Add a table at the bottom of the axes
    the_table = plt.table(cellText=cell_text,
                        rowLabels=rows,
                        rowColours=colors,
                        colLabels=columns,
                        loc='bottom')
    the_table.set_fontsize(12)
    the_table.scale(1, 1.9)

    # Adjust layout to make room for the table:
    plt.subplots_adjust(left=0.2, bottom=0.35)

    plt.ylabel("Combined score")
    plt.yticks(values * value_increment, ['%f' % val for val in values])
    plt.xticks([])
    plt.title('Score per iteration')

    plt.show()

# print off the stats of the base classifiers and show the graph
print("*****************************Base classifiers*****************************")
make_graph()
print("**************************************************************************")

# print off the stats of the classifiers modified by grid search and show the graph
print("***********************Classifiers using grid search***********************")
make_graph(method=GridSearchCV)
print("***************************************************************************")

